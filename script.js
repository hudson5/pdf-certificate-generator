const buttonGerarCertificado = document.querySelector('button');
const inputNomeCandidato = document.querySelector('input[type="text"]');
const inputEmailCandidato = document.querySelector('input[type="email"]');

function Script1(nomeCandidato) {
  let date = new Date();
  let dd = String(date.getDate()).padStart(2, '0');
  let mm = String(date.getMonth() + 1).padStart(2, '0'); 
  let yyyy = date.getFullYear();
date = mm + '/' + dd + '/' + yyyy;

// let name = "Hudson Carolino"; /* Remove this line of code if your certificate is in portrait orientation */
let doc = new jsPDF({
  orientation: 'landscape' /* Remove this line of code if your certificate is in portrait orientation */
})
   



let img = new Image;
img.onload = function() {
  doc.addImage(this, 0, 0, 297, 210);
  doc.setFontSize(32); /* Set the font size by changing the number between the parentheses */
  doc.setTextColor(255, 255, 255); /* Change the RGB text color by changing the numbers between the parentheses */
  doc.setFont('Helvetica', 'bold');
  doc.text(nomeCandidato, (doc.internal.pageSize.width / 2), 120, null, null, 'center'); /* This tells the PDF to add text to the page. The first argument is the text to add (in this case a JavaScript variable), the second is the x-coordinate (in this case weâ€™re using a function to find a middle point for the document), the third is the y-coordinate (in millimeters), the fourth and fifth arguments are null, and the sixth argument says that the text should be centered */
   doc.setFont('Helvetica', 'bold');
   doc.setFontSize(15);
   doc.text(date, (doc.internal.pageSize.width / 2), 172, null, null, 'center'); /* See above for the full explanation of the doc.text() function */
   doc.save("Certificate.pdf"); /* Swap out â€˜Certificateâ€™ with what you want your certificate to be named */
  };
img.crossOrigin = "";  
img.src = "certificate.jpg"; /* Change the file name between these quotes to the file that you exported as the base certificate */

document.cookie = 'nome=0aopython2; expires=; path=/';
}

buttonGerarCertificado.addEventListener('click', function(){
  
  verificarCookies();
})

async function validateEmail(emailCandidato) {
  return await fetch('./csvjson.json')
    .then(response => response.json())
    .then(data => data.some((item) => item["Email Usuário"] === emailCandidato))
    .then(retorno => {
      if(retorno){
        const nome = inputNomeCandidato.value;
        if(nome.trim() !== '' && nome.trim() !== undefined && nome.trim() !== null){
          Script1(nome);
          mensagemSucesso('Certificado gerado com sucesso!')
          console.log(true)
        }else{
          mensagemFalha('Preencha os campos corretamente.');
        }
      }else{
        mensagemFalha('Email não cadastrado.');
        console.error('Você não pode gerar o certificado')
      }
    })
    .catch(err => console.error(err));
}

function verificarCookies(){
  const strCookies = document.cookie;
  const arrCookies = strCookies.split('; ');

  if(arrCookies.includes('nome=0aopython2') === false){
    const email = inputEmailCandidato.value;
    validateEmail(email)
    console.log(arrCookies);
  } else{
    mensagemFalha('Você só pode baixar o certificado uma vez.');
    console.log('já baixou o certificado');
  }
}

function mensagemFalha(mensagem){
  const span = document.querySelector('span');
  span.innerText = mensagem;
  span.classList.remove('mostrarMensagem--falha', 'mostrarMensagem--sucesso');
  span.classList.add('mostrarMensagem--falha');
}

function mensagemSucesso(mensagem){
  const span = document.querySelector('span');
  span.innerText = mensagem;
  span.classList.remove('mostrarMensagem--falha', 'mostrarMensagem--sucesso');
  span.classList.add('mostrarMensagem--sucesso');
}